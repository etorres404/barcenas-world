#!/usr/bin/env python

"""Barcenas World main program.

This program has two main calls:

    - ./barcenas-world n steps ->
        Generates a Barcenas World program with n x n positions and Runs
        the steps on this world.

    - ./barcenas-world n -i|--interactive ->
        Generates a Barcenas World program with n x n positions and enters on
        interactive mode, where you can run steps queries several times without
        closing the program.
"""


import dynamic  # Dynamic knowledge, different depending on world size
import argparse
import os
import sys
import readline

__author__ = 'Eduard Torres Montiel (etorres404@gmail.com)'


def create_prolog_file(n):
    """Generalization of 3x3 Barcenas World to NxN Barcenas World."""
    program = "\n"
    program += dynamic.smell_sensor_info(n)
    program += dynamic.mariano_info(n)
    with open('template.pl', 'r') as f:
        program += f.read()
    # We need to generalize the first step too
    program += dynamic.first_state(n)
    return program


def file_checking():
    """Checking of template.pl and init.pl existance."""
    # Check if template.pl file exists
    if not os.path.isfile('./template.pl'):
        print "ERROR! template.pl not found! Necessary file."
        sys.exit(-1)
    # Check if init.pl file exists
    if not os.path.isfile('./init.pl'):
        print "ERROR! init.pl not found! Necessary file."
        sys.exit(-2)


def execute_query(steps):
    """Execution of steps query on program.pl."""
    query = 'execSeqofSteps([%s], F).' % steps
    logic_program = 'program.pl'
    shell_command = 'swipl -q -s init.pl -f ' + logic_program + ' -t \'' + \
                    query + '\' 2> /dev/null'
    return os.system(shell_command)


def interactive_mode():
    """Run of the interactive mode of the application."""
    print '--------------------------------------------------------------'
    print '* Welcome to interactive mode in barcenas-world application  *'
    print '* Here you can enter several queries with the same format    *'
    print '* than with non interactive mode without closing the program *'
    print '--------------------------------------------------------------'
    cmd = '--> Enter a set of steps or the word exit'
    print cmd
    readline.parse_and_bind('tab: complete')
    steps = raw_input('>>> ')
    while steps != 'exit':
        response = execute_query(steps)
        if response == 1:
            print '--> Something wrong happened!!'
        print cmd
        steps = raw_input('>>> ')
    else:
        print 'Program finished.'

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Barcenas world simulator.')
    parser.add_argument('n', type=int, help='Size of the word')
    parser.add_argument('steps', type=str, nargs='?',
                        default='[]', help='Steps to execute')
    parser.add_argument('--interactive', '-i', action='store_true',
                        required=False, default=False,
                        help='Runs application on interactive mode (optional)')
    args = parser.parse_args()
    n = args.n
    steps = args.steps
    # Create dynamic clauses in function of n
    program = create_prolog_file(n)
    # Append dynamic clauses to program.pl
    with open('program.pl', 'w') as f:
        f.write(program)
    if args.interactive:
        try:
            interactive_mode()
        except KeyboardInterrupt:
            print '\nProgram finished.'
    else:
        # Execution of query
        execute_query(steps)
    # Finally removes prolog program
    os.remove('program.pl')
