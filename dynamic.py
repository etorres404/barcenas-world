#!/usr/bin/env python

"""Dynamic generation of clauses in Barcenas World program.

Depends of world's size.

This file has three public functions:
    - smell_sensor_info(n)
    - mariano_info(n)
    - first_state(n)
"""

__author__ = 'Eduard Torres Montiel (etorres404@gmail.com)'


def _mariano_info_right(n):
    program = ""

    # Mariano gives as possible positions of Barcenas only the ones which
    # are on right or on the same column of Mariano

    # Iteration over all world columns
    for j in xrange(1, n+1):
        # Barcenas could be anywhere
        positions = [[1 for _ in xrange(n)] for _ in xrange(n)]
        # Iteration over position list
        for x in xrange(n):
            for y in xrange(n):
                if y + 1 >= j and (x+1, y+1) != (1, 1):
                    positions[x][y] = 1
                else:
                    positions[x][y] = 0
        program += 'isMariano({0}, 0, [{1}]).\n' \
                   .format(j, ','.join(map(str, positions)))
    return program


def _mariano_info_left(n):
    program = ""

    # Mariano gives as possible positions of Barcenas only the ones which
    # are on left of Mariano

    # Iteration over all world columns
    for j in xrange(1, n+1):
        # Barcenas could be anywhere
        positions = [[1 for _ in xrange(n)] for _ in xrange(n)]
        # Iteration over position list
        for x in xrange(n):
            for y in xrange(n):
                if y + 1 < j and (x+1, y+1) != (1, 1):
                    positions[x][y] = 1
                else:
                    positions[x][y] = 0
        program += 'isMariano({0}, 1, [{1}]).\n' \
                   .format(j, ','.join(map(str, positions)))
    return program


def _smell_sensor_not_smells(n):
    program = ""

    # isBarcenasAround when Smell = 1, gives as possible locs
    # only the ones around the current pos

    # Iteration over all world positions
    for i in xrange(1, n+1):
        for j in xrange(1, n+1):
            # Generation of places where Barcenas can be
            barcenas = [(i, j), (i+1, j), (i-1, j), (i, j+1), (i, j-1)]
            # Barcenas could be anywhere
            positions = [[1 for _ in xrange(n)] for _ in xrange(n)]
            # Iteration over position list
            for x in xrange(n):
                for y in xrange(n):
                    if (x+1, y+1) in barcenas and (x+1, y+1) != (1, 1):
                        positions[x][y] = 1
                    else:
                        positions[x][y] = 0
            # When list is completed we can add the clause to the program
            program += 'isBarcenasAround({0}, {1}, 1, [{2}]).\n' \
                       .format(i, j, ','.join(map(str, positions)))
    return program


def _smell_sensor_smells(n):
    program = ""

    # isBarcenasAround when Smell = 0, gives as possible locs
    # only the ones not around the current pos

    # Iteration over all world positions
    for i in xrange(1, n+1):
        for j in xrange(1, n+1):
            # Generation of places where Barcenas can not be
            not_barcenas = [(1, 1), (i, j), (i+1, j),
                            (i-1, j), (i, j+1), (i, j-1)]
            # Barcenas could be anywhere
            positions = [[1 for _ in xrange(n)] for _ in xrange(n)]
            # Iteration over position list
            for x in xrange(n):
                for y in xrange(n):
                    if (x+1, y+1) in not_barcenas:
                        positions[x][y] = 0
                    else:
                        positions[x][y] = 1
            # When list is completed we can add the clause to the program
            program += 'isBarcenasAround({0}, {1}, 0, [{2}]).\n' \
                       .format(i, j, ','.join(map(str, positions)))
    return program


def smell_sensor_info(n):
    """Smell sensor information, it depends of world size."""
    program = "/* DYNAMIC INFORMATION GENERATED THROUGH barcenas-world.py */\n"
    program += _smell_sensor_smells(n)
    program += '\n'
    program += _smell_sensor_not_smells(n)
    program += '\n'
    return program


def mariano_info(n):
    """Mariano information, it depends of world size."""
    program = ""
    # Mariano not found
    program += 'isMariano( _, -1, [[1,1,1],[1,1,1],[1,1,1]] ).\n\n'
    # Mariano found and Barcenas on left
    program += _mariano_info_left(n)
    program += '\n'
    # Mariano found and Barcenas on right
    program += _mariano_info_right(n)
    program += '\n'
    return program


def first_state(n):
    """First state of program generation depending of n."""
    first_state = [[1 for _ in xrange(n)] for _ in xrange(n)]
    first_state[0][0] = 0
    return "    execSeqofStepsRec( %s, -1, -1, -1, -1, Steps, FinalStep ).\n" \
           % first_state
