# BARCENAS WORLD #

Simple application programmed in the logical programming language *Prolog* and extended with a simple script in *Python*.

This application is the first assignment of [Automated Learning and Reasoning](http://guiadocent.udl.cat/pdf/en/102040) subject, and it is based on a class problem, explained in the next sections.

## Table of Contents ##

* [Problem description](#problem-description)
* [Requirements](#requirements)
* [How to run](#how-to-run)
    * [Required Files](#required-files)
    * [Steps input](#steps-input)
    * [Execution modes](#execution-modes)

## Problem description

Firstly, we are going to explain the simple Barcenas problem. This is the basic problem, and the first implementation of it in *Prolog* was given in class as example.

> We are an intelligent (but blind) agent that we start in some known point of a NxN world. In this world there is a big monster called Barcenas in some unknow position, and we have the restriction that he can not move. In order to find him, we have a smell sensor that tells us is if Barcenas is arround our position (i.e. if Barcenas is on the same position as the agent, on right, on left, in front or behind) or if he is not (he is not in any of the five positions explained before). Our goal is to discard positions and find Barcenas.

Next, the problem goes harder...

> In the world we were, some other characters have appeared: 
> * Mariano, a character who tells us if Barcenas is on his left if we find him
> * Cospedal, a character who tells us if Mariano is lying or not if we find her
> * Rita, a character who tells is if Cospedal is lying or not, if we find her
> The knowledge provided for this three characters will only be useful when we have found all of them, and we can find them in any order, and maybe several times.

## Requirements

* [Prolog interpreter](http://www.swi-prolog.org/)
* [Python 2.7.11](https://www.python.org/)

## How to run

### Required files

To execute it, the following files are required:

* barcenas-world.py
* dynamic.py
* init.pl
* template.pl

The file *testSimpleBarcenasLocatrions.pl* is not required. This file is a pure Prolog functional application of a 3x3 Barcenas World.

## Steps input

This is a reasoning and learning solution, and it does not do any search. Giving a sequence of steps, the program reasons where could not be Barcenas. A possible improvement could be the automatic search of the best place to move our agent in order to find where is Barcenas optimally.

In order to query the agent, a sequence of steps must be created. This sequence is a list of a list, with this specific format:

`[[X, Y, S, M, C, R], ...]`

* **X**: agent X coordinate.
* **Y**: agent Y coordinate.
* **S**: smell sensor information on (X, Y) position. 0 means that does not sell, 1 that does.
* **M**: Mariano variable. It could be -1 if Mariano is not found in (X, Y), 0 if it is found and Barcenas is on his right or 1 if it is found and Barcenas is on his left.
* **C**: Cospedal variable. It could be -1 if Cospedal is not found in (X, Y), 0 if Mariano is not lying and 1 if he is lying.
* **R**: Rita variable: It could be -1 if Rita is not found in (X, Y), 0 if Cospedal is not lying and 1 if she is lying.

The program assumes that the sequence of steps is valid, and there are no contradictions on it.

### Execution modes

The application has two different execution modes:

* **Command mode**: `python barcenas-world.py N Steps`
* **Interpret mode**: `python barcenas-world.py N -i`

Where **N** is the size of the world, and **Steps** are the sequence of steps followed by the agent.

On the interpret mode, the steps are introduced in the command line, and several sequences of steps can be queried on the same world, without having to generate it for every query.