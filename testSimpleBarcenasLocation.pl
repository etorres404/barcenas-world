/* :- use_module(library(clpfd)).  */

/*
 Simple Barcenas World:

  3 x 3 grid, each one can generate a smell signal

  Rule :
                                                        i+1,j
    smell in i,j => Barcenas can be in locations  i,j-1  i,j  i,j+1
                                                        i-1,j
    except in initial location of the agent: (1,1)
*/

/* Intersect Prev Info about a location of Barcenas (first argument)
   with a new Info for the same location (second argument). The resulting updated
   information for the location is in the third argument.
   Observe that with this "programming" of intersectLocInfo some answers can be
   obtained in different ways. */

intersectLocInfo( 0, _, 0 ).
intersectLocInfo( _, 0, 0 ).
intersectLocInfo( 1, Y, Y ).
intersectLocInfo( X, 1, X ).

intersectRow( [], [], [] ).

intersectRow( [PH|PT], [NH|NT], [FH|FT] ) :-
             intersectLocInfo( PH, NH, FH ),
             intersectRow( PT, NT, FT ).

intersectLocs( [], [], [] ).

intersectLocs( [PrevRow|PrevLocs], [NewRow|NewLocs], FinalLocs ) :-
             intersectRow( PrevRow, NewRow, FinalRow ),
             intersectLocs( PrevLocs, NewLocs, RestOfRows ),
             FinalLocs = [ FinalRow | RestOfRows ].


/* isBarcenasAround when Smell = 0, gives as possible locs
 only the ones not around the current pos   !!! */


isBarcenasAround( 1, 1, 0, [[0,0,1],[0,1,1],[1,1,1]] ).
isBarcenasAround( 1, 2, 0, [[0,0,0],[1,0,1],[1,1,1]] ).
isBarcenasAround( 1, 3, 0, [[0,0,0],[0,1,0],[1,1,1]] ).
isBarcenasAround( 2, 1, 0, [[0,1,1],[0,0,1],[0,1,1]] ).
isBarcenasAround( 2, 2, 0, [[0,0,1],[0,0,0],[1,0,1]] ).
isBarcenasAround( 2, 3, 0, [[0,1,0],[1,0,0],[1,1,0]] ).
isBarcenasAround( 3, 1, 0, [[0,1,1],[0,1,1],[0,0,1]] ).
isBarcenasAround( 3, 2, 0, [[0,1,1],[1,0,1],[0,0,0]] ).
isBarcenasAround( 3, 3, 0, [[0,1,1],[1,1,0],[1,0,0]] ).

/* isBarcenasAround when Smell = 1, gives as possible locs only the ones around
    the agent current position  */
isBarcenasAround( 1, 1, 1, [[0,1,0],[1,0,0],[0,0,0]] ).
isBarcenasAround( 1, 2, 1, [[0,1,1],[0,1,0],[0,0,0]] ).
isBarcenasAround( 1, 3, 1, [[0,1,1],[0,0,1],[0,0,0]] ).
isBarcenasAround( 2, 1, 1, [[0,0,0],[1,1,0],[1,0,0]] ).
isBarcenasAround( 2, 2, 1, [[0,1,0],[1,1,1],[0,1,0]] ).
isBarcenasAround( 2, 3, 1, [[0,0,1],[0,1,1],[0,0,1]] ).
isBarcenasAround( 3, 1, 1, [[0,0,0],[1,0,0],[1,1,0]] ).
isBarcenasAround( 3, 2, 1, [[0,0,0],[0,1,0],[1,1,1]] ).
isBarcenasAround( 3, 3, 1, [[0,0,0],[0,0,1],[0,1,1]] ).

/*  From the list of prev possible locs of Barcenas in PrevLocs,
    and the new info from agent (X,Y,Smell) get the
     new set of possible locations in FinalLocs

For example, starting from the initial state where every location
, except 1,1, is possible:
 updatePosBarcenasLocs( [[0,1,1],[1,1,1],[1,1,1]], 1, 1, 0, F ).
 we get F = [[0,0,1],[0,1,1],[1,1,1]]

if we next  move to 1,2 and it still does not smell:
 updatePosBarcenasLocs( [[0,0,1],[0,1,1],[1,1,1]], 1, 2, 0, F ).
 we get as the next state F:
   F =  [[0,0,0],[0,0,1],[1,1,1]]

 if we next move to 2,2 and it still does not smell:
 updatePosBarcenasLocs( [[0,0,0],[0,0,1],[1,1,1]], 2, 2, 0, F ).
 we get as the next state F:
   F =  [[0,0,0],[0,0,0],[1,0,1]]

   We can execute a query where we execute a sequence of consecutive steps, where the
    resulting state of a step is the previous state for the next step, in the following
    way (it must be put in the prolog interpreter in a SINGLE line):

     updatePosBarcenasLocs( [[0,1,1],[1,1,1],[1,1,1]], 1, 1, 0, F1 ),
      updatePosBarcenasLocs( F1, 1, 2, 0, F2 ),
       updatePosBarcenasLocs( F2, 2, 2, 0, F3 ),
         updatePosBarcenasLocs( F3, 2, 3, 1, F4 ).

  In this example query, we are executing the sequence of steps (with smell readings:)

  1,1,0
  1,2,0
  2,2,0
  2,3,1

  that gives as final state:

  F4 = [[0, 0, 0], [0, 0, 0], [0, 0, 1]]

  Try to program a recursive predicate, called execSeqofSteps( [Step1,Step2, ... , StepN], FinalState ),
   that given a list of steps with the format:

    [Step1,Step2, ... , StepN]  where each step is also a list: [X,Y,SmellValue]

    it will execute that sequence of steps starting from the initial state
     [[0,1,1],[1,1,1],[1,1,1]]

*/

/* Mariano not found */
isMariano( _, -1, [[1,1,1],[1,1,1],[1,1,1]] ).

/* Mariano Found and Barcenas is on left */
isMariano( 1, 1, [[0,0,0],[0,0,0],[0,0,0]] ). /* This might not happen */
isMariano( 2, 1, [[1,0,0],[1,0,0],[1,0,0]] ).
isMariano( 3, 1, [[1,1,0],[1,1,0],[1,1,0]] ).

/* Mariano Found and Barcenas is on right */
isMariano( 1, 0, [[1,1,1],[1,1,1],[1,1,1]] ).
isMariano( 2, 0, [[0,1,1],[0,1,1],[0,1,1]] ).
isMariano( 3, 0, [[0,0,1],[0,0,1],[0,0,1]] ).


/* ANALYSIS */
/* Cospedal says Mariano lies, Rita says Cospedal lies
(Mariano not lies) M 1 1 Y */
analysis(M, 1, 1, Y, PrevLocs, FinalLocs):-
    isMariano(Y, M, Locs),
    intersectLocs(PrevLocs, Locs, FinalLocs).

/* Cospedal says Mariano not lies, Rita says Cospedal lies
(Mariano lies) M 0 1 Y */
analysis(M, 0, 1, Y, PrevLocs, FinalLocs):-
    invertMariano(M, Inv_M),
    isMariano(Y, Inv_M, Locs),
    intersectLocs(PrevLocs, Locs, FinalLocs).

/* Cospedal says Mariano lies, Rita says Cospedal not lies
(Mariano lies) M 1 0 Y */
analysis(M, 1, 0, Y, PrevLocs, FinalLocs):-
    invertMariano(M, Inv_M),
    isMariano(Y, Inv_M, Locs),
    intersectLocs(PrevLocs, Locs, FinalLocs).

/* Cospedal says Mariano not lies, Rita says Cospedal not lies
(Mariano not lies) M 0 0 Y */
analysis(M, 0, 0, Y, PrevLocs, FinalLocs):-
    isMariano(Y, M, Locs),
    intersectLocs(PrevLocs, Locs, FinalLocs).

/* Cospedal or Rita or Mariano not found */
analysis(-1, _, _, _, PrevLocs, PrevLocs).

analysis(_, -1, _, _, PrevLocs, PrevLocs).

analysis(_, _, -1, _, PrevLocs, PrevLocs).

/* Utility to invert mariano response */
invertMariano(1, 0).
invertMariano(0, 1).

/* Pretty print of result */
pprint(Locs):-
    reverse(Locs, RLocs),
    print_locs(RLocs).

reverse(Locs, RLocs):-
    reverse(Locs, [], RLocs).

reverse([], A, A).
reverse([H|T], A, R):-
    reverse(T, [H|A], R).

print_locs([]):- !.
print_locs([H|T]):- write(H), nl, print_locs(T).

updatePosBarcenasLocs( PrevLocs, MarianoMem, MarianoCoord, CospedalMem, RitaMem,
                       AgentPosX, AgentPosY, SmellXY, Mariano, Cospedal, Rita, FinalLocs ):-
      /* Smell Sensor */
      isBarcenasAround( AgentPosX, AgentPosY, SmellXY, NewLocs ),
      intersectLocs( PrevLocs, NewLocs, LocsSmellSensor ), !,
      /* Analysis of mariano t-1, cospedal t and rita t-1 */
      analysis( MarianoMem, Cospedal, RitaMem, MarianoCoord, LocsSmellSensor, NewLocs1 ),
      /* Analysis of mariano t-1, cospedal t-1 and rita t */
      analysis( MarianoMem, CospedalMem, Rita, MarianoCoord, NewLocs1, NewLocs2 ),
      /* Analysis of mariano t-1, cospedal t and rita t */
      analysis( MarianoMem, Cospedal, Rita, MarianoCoord, NewLocs2, NewLocs3 ),
      /* Analysis of mariano t, cospedal t-1 and rita t-1 */
      analysis( Mariano, CospedalMem, RitaMem, AgentPosY, NewLocs3, NewLocs4 ),
      /* Analysis of mariano t, cospedal t, rita t-1 */
      analysis( Mariano, Cospedal, RitaMem, AgentPosY, NewLocs4, NewLocs5 ),
      /* Analysis of mariano t, cospedal t-1, rita t */
      analysis( Mariano, CospedalMem, Rita, AgentPosY, NewLocs5, NewLocs6 ),
      /* Analysis of mariano t, cospedal t, rita t */
      analysis( Mariano, Cospedal, Rita, AgentPosY, NewLocs6, FinalLocs ), !,
      write( 'Estado resultante: ' ), nl, pprint( FinalLocs ), nl.

/* In this updatePosBarcenasLocs, there is a strange predicate symbol: ! . It is
used to "cut" the many possible alternative proof branches that can be obtained because
for some answers the predicate intersectLocs can give the same answer with different
resolution proofs, and because we are not interested in obtaining the same answer with
different branches we can cut the secondary ones with this symbol, that breaks off
the backtracking that would allow  intersectLocs to be resolved in more  than one way.
Try to erase the ! symbol and observe that now the interpreter will give you the same
answer different times.  */

/* Update t-1 vars with current information in case that it information ampliates
our knowledge (i.e Mariano not found yet and now we foud him; same case for Cospedal)*/

/* Mariano is found now. We update our knowledge and we will check in next iteration
if we have all the necessary information to discard some Barcenas positions*/
marianoFoundNow( 1, Y, _, _, 1, Y).
marianoFoundNow( 0, Y, _, _, 0, Y).

/* If Mariano is not found in time t, we will mantain the previous information
that we have untill now, because the new information does not extend our knowledge */
marianoFoundNow( -1, _, M, Y, M, Y).

/* In Cospedal and Rita case, if we have found her we mantain her response, because our model
is consistent and if we find her on a future, her response will be the same */
cospedalOrRitaFoundNow( _, 1, 1 ).
cospedalOrRitaFoundNow( _, 0, 0 ).

/* But we need to store Cospedal's or Rita's first response */
cospedalOrRitaFoundNow( 1, -1, 1 ).
cospedalOrRitaFoundNow( 0, -1, 0 ).

/* And in case that we do not find Cospedal or Rita yet, and we do not find her now,
we store that we have not find Cospedal (or Rita)*/
cospedalOrRitaFoundNow( -1, -1, -1 ).

/* Main recursion of program */
execSeqofStepsRec( FirstState, _, _, _, _, [], FirstState ) :- !.

execSeqofStepsRec( FirstState, MarianoMem, MarianoCoord, CospedalMem,
                   RitaMem, [[X,Y,S,M,C,R] | StepsT], FinalState ) :-
	updatePosBarcenasLocs( FirstState, MarianoMem, MarianoCoord, CospedalMem,
                           RitaMem, X, Y, S, M, C, R, F ),
    marianoFoundNow( M, Y, MarianoMem, MarianoCoord, NewMariano, NewCoord ),
    cospedalOrRitaFoundNow( C, CospedalMem, NewCospedal),
    cospedalOrRitaFoundNow( R, RitaMem, NewRita ),
	execSeqofStepsRec( F, NewMariano, NewCoord, NewCospedal, NewRita, StepsT, FinalState ).

/* First function call */
execSeqofSteps( Steps, FinalStep ) :-
	execSeqofStepsRec( [[0,1,1],[1,1,1],[1,1,1]], -1, -1, -1, -1, Steps, FinalStep ).

/* Tests that have result [[0, 0, 0], [0, 0, 0], [1, 0, 0]]:
execSeqofSteps([[1,1,0,-1,-1,-1],[1,2,0,1,-1,-1],[1,3,0,-1,1,1],[2,3,0,-1,-1,-1]], F).
execSeqofSteps([[1,1,0,-1,-1,-1],[1,2,0,1,1,1],[1,3,0,-1,-1,-1],[2,3,0,-1,-1,-1]], F).
execSeqofSteps([[1,1,0,-1,-1,-1],[1,2,0,1,-1,-1],[1,3,0,-1,-1,1],[2,3,0,-1,1,-1]], F).
execSeqofSteps([[1,1,0,-1,-1,-1],[1,2,0,1,-1,-1],[1,3,0,-1,1,-1],[2,3,0,-1,-1,1]], F).
execSeqofSteps([[1,1,0,-1,-1,-1],[1,2,0,1,-1,-1],[1,3,0,-1,0,-1],[2,3,0,-1,-1,0]], F).
*/
