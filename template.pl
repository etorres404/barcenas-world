/* AUTHOR: Eduard Torres Montiel (etorres404@gmail.com)*/

/* TEMPLATE OF BARCENAS WORLD PROGRAM.
HERE YOU CAN FIND ALL THE STATIC CLAUSES
THAT DO NOT DEPEND OF WORLD SIZE*/

/* Intersect Prev Info about a location of Barcenas (first argument)
   with a new Info for the same location (second argument). The resulting updated
   information for the location is in the third argument.
   Observe that with this "programming" of intersectLocInfo some answers can be
   obtained in different ways. */

intersectLocInfo( 0, _, 0 ).
intersectLocInfo( _, 0, 0 ).
intersectLocInfo( 1, Y, Y ).
intersectLocInfo( X, 1, X ).

intersectRow( [], [], [] ).

intersectRow( [PH|PT], [NH|NT], [FH|FT] ) :-
             intersectLocInfo( PH, NH, FH ),
             intersectRow( PT, NT, FT ).

intersectLocs( [], [], [] ).

intersectLocs( [PrevRow|PrevLocs], [NewRow|NewLocs], FinalLocs ) :-
             intersectRow( PrevRow, NewRow, FinalRow ),
             intersectLocs( PrevLocs, NewLocs, RestOfRows ),
             FinalLocs = [ FinalRow | RestOfRows ].

/* ANALYSIS */
/* Cospedal says Mariano lies, Rita says Cospedal lies
(Mariano not lies) M 1 1 Y */
analysis(M, 1, 1, Y, PrevLocs, FinalLocs):-
    isMariano(Y, M, Locs),
    intersectLocs(PrevLocs, Locs, FinalLocs).

/* Cospedal says Mariano not lies, Rita says Cospedal lies
(Mariano lies) M 0 1 Y */
analysis(M, 0, 1, Y, PrevLocs, FinalLocs):-
    invertMariano(M, Inv_M),
    isMariano(Y, Inv_M, Locs),
    intersectLocs(PrevLocs, Locs, FinalLocs).

/* Cospedal says Mariano lies, Rita says Cospedal not lies
(Mariano lies) M 1 0 Y */
analysis(M, 1, 0, Y, PrevLocs, FinalLocs):-
    invertMariano(M, Inv_M),
    isMariano(Y, Inv_M, Locs),
    intersectLocs(PrevLocs, Locs, FinalLocs).

/* Cospedal says Mariano not lies, Rita says Cospedal not lies
(Mariano not lies) M 0 0 Y */
analysis(M, 0, 0, Y, PrevLocs, FinalLocs):-
    isMariano(Y, M, Locs),
    intersectLocs(PrevLocs, Locs, FinalLocs).

/* Cospedal or Rita or Mariano not found */
analysis(-1, _, _, _, PrevLocs, PrevLocs).

analysis(_, -1, _, _, PrevLocs, PrevLocs).

analysis(_, _, -1, _, PrevLocs, PrevLocs).

/* Utility to invert mariano response */
invertMariano(1, 0).
invertMariano(0, 1).

/* Pretty print of result */
pprint(Locs):-
    reverse(Locs, RLocs),
    print_locs(RLocs).

reverse(Locs, RLocs):-
    reverse(Locs, [], RLocs).

reverse([], A, A).
reverse([H|T], A, R):-
    reverse(T, [H|A], R).

print_locs([]):- !.
print_locs([H|T]):- write(H), nl, print_locs(T).

updatePosBarcenasLocs( PrevLocs, MarianoMem, MarianoCoord, CospedalMem, RitaMem,
                       AgentPosX, AgentPosY, SmellXY, Mariano, Cospedal, Rita, FinalLocs ):-
      /* Smell Sensor */
      isBarcenasAround( AgentPosX, AgentPosY, SmellXY, NewLocs ),
      intersectLocs( PrevLocs, NewLocs, LocsSmellSensor ), !,
      /* Analysis of mariano t-1, cospedal t and rita t-1 */
      analysis( MarianoMem, Cospedal, RitaMem, MarianoCoord, LocsSmellSensor, NewLocs1 ),
      /* Analysis of mariano t-1, cospedal t-1 and rita t */
      analysis( MarianoMem, CospedalMem, Rita, MarianoCoord, NewLocs1, NewLocs2 ),
      /* Analysis of mariano t-1, cospedal t and rita t */
      analysis( MarianoMem, Cospedal, Rita, MarianoCoord, NewLocs2, NewLocs3 ),
      /* Analysis of mariano t, cospedal t-1 and rita t-1 */
      analysis( Mariano, CospedalMem, RitaMem, AgentPosY, NewLocs3, NewLocs4 ),
      /* Analysis of mariano t, cospedal t, rita t-1 */
      analysis( Mariano, Cospedal, RitaMem, AgentPosY, NewLocs4, NewLocs5 ),
      /* Analysis of mariano t, cospedal t-1, rita t */
      analysis( Mariano, CospedalMem, Rita, AgentPosY, NewLocs5, NewLocs6 ),
      /* Analysis of mariano t, cospedal t, rita t */
      analysis( Mariano, Cospedal, Rita, AgentPosY, NewLocs6, FinalLocs ), !,
      write( 'Result state: ' ), nl, pprint( FinalLocs ), nl.

/* Update t-1 vars with current information in case that it information ampliates
our knowledge (i.e Mariano not found yet and now we foud him; same case for Cospedal)*/

/* Mariano is found now. We update our knowledge and we will check in next iteration
if we have all the necessary information to discard some Barcenas positions*/
marianoFoundNow( 1, Y, _, _, 1, Y).
marianoFoundNow( 0, Y, _, _, 0, Y).

/* If Mariano is not found in time t, we will mantain the previous information
that we have untill now, because the new information does not extend our knowledge */
marianoFoundNow( -1, _, M, Y, M, Y).

/* In Cospedal and Rita case, if we have found her we mantain her response, because our model
is consistent and if we find her on a future, her response will be the same */
cospedalOrRitaFoundNow( _, 1, 1 ).
cospedalOrRitaFoundNow( _, 0, 0 ).

/* But we need to store Cospedal's or Rita's first response */
cospedalOrRitaFoundNow( 1, -1, 1 ).
cospedalOrRitaFoundNow( 0, -1, 0 ).

/* And in case that we do not find Cospedal or Rita yet, and we do not find her now,
we store that we have not find Cospedal (or Rita)*/
cospedalOrRitaFoundNow( -1, -1, -1 ).

/* Main recursion of program */
execSeqofStepsRec( FirstState, _, _, _, _, [], FirstState ) :- !.

execSeqofStepsRec( FirstState, MarianoMem, MarianoCoord, CospedalMem,
                   RitaMem, [[X,Y,S,M,C,R] | StepsT], FinalState ) :-
	updatePosBarcenasLocs( FirstState, MarianoMem, MarianoCoord, CospedalMem,
                           RitaMem, X, Y, S, M, C, R, F ),
    marianoFoundNow( M, Y, MarianoMem, MarianoCoord, NewMariano, NewCoord ),
    cospedalOrRitaFoundNow( C, CospedalMem, NewCospedal),
    cospedalOrRitaFoundNow( R, RitaMem, NewRita ),
	execSeqofStepsRec( F, NewMariano, NewCoord, NewCospedal, NewRita, StepsT, FinalState ).

/* First function call */
execSeqofSteps( Steps, FinalStep ) :-
	/* Generalization of first step, generated by barcenas-world.py */
